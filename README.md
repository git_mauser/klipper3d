# Klipper3D

This project contains information on how to: 

- install klipper3D (https://www.klipper3d.org/) on a Raspi4
- install libraries required to install and run klipper3D
- configure Arduino's to interface with klipper3D
- create and configure a Printer.cfg file
