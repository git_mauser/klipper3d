# Configuring a Printer.cfg for use with Klipper3d

### Objective: To create and understand the creation of a printer.cfg file and the variables used.

#### **Caution: Some variables are calibrated for specific hardware and printer types. This guide refers to the setup we used.**

#### Main Sources of Documentation: [Klipper](https://www.klipper3d.org/), [Klipper Github](https://github.com/KevinOConnor/klipper) and [TMC2130 Data Sheet](http://www.farnell.com/datasheets/3119137.pdf)

#### NOTE: ! - defines an inverted pin, ^ - defines that an internal pullup resistor is to be used

#### **Guide:**
---

* **Step 1:** Create a file using Vim or similar in your home directory, called printer.cfg. It is viable to use the templates provided by klipper, `cp ~/klipper/config/example-cartesian.cfg ~/printer.cfg`.The printer configuration file is searched for by klipper and is essential for klipper to communicate with hardware and establish the parameters necessary to successfully run your printer.

---

* **Step 2:** Once the printer.cfg file has been created, ascertain the hardware that is connected to your raspberry pi. In our case 3+ arduino nanos. The follwing command will list the devices currently connected via USB - `ls -la /dev/serial/by-path/*`

---

* **Step 3:** Using the output from the command in step 2, it is now time to define the MCU or main control unit, in our case we are using a multi MCU setup for finer control and modularity. See below for an example of how to define an MCU.


[mcu] **- Define the name of an MCU. All printer.cfg files must contain a Main MCU definition with no added naming. This is our X axis**

serial: /dev/serial/by-path/platform-fd500000.pcie-pci-0000:01:00.0-usb-0:1.4:1.0-port0 **- Define the serial connection used**

pin_map: arduino **- Define the pin mapping to be used. This establishes the naming convention used later in the printer.cfg file**

baud: 56700 **- State the baud rate that is to be used for communication. We have used the same baud rate used when flashing the arduinos**

restart_method: arduino **- Define the method klipper will use when restarting the MCU during printer use**

---

[mcu yboard]

serial: dev/serial/by-path/platform-fd500000.pcie-pci-0000:01:00.0-usb-0:1.3:1.0-port0

pin_map: arduino

baud: 56700

restart_method: arduino

**Note that the above is an additional MCU assignment. The naming of additional MCU's must follow the convention - [mcu (anything you like)]. The second part of the name is used throughout the printer.cfg file to define parameters associated with this particualr MCU. The above is our Y axis. We also have an Z axis MCU and an Extruder MCU yet to be defined.**

---

* **Step 4:** The next step is to define the variables that will be used by Klipper to setup each MCU during startup. In turn the MCU will use these definitions to communicate with the Stepper driver. Be sure to use simple and self explanitory naming for these sections as they are used throughout the printer.cfg file. Please see the examples below and note the assignments given to the pins when using multiple MCU. NOTE: ! - defines an inverted pin, ^ - defines that an internal pullup resistor is to be used

[stepper_x] **- The name of Stepper Motor or driver that is to be controlled. Naming must follow the convention [stepper_(anything)]**

step_pin: ar6 **- This is the pin on our arduino that will be used to communicate the steps**

dir_pin: !ar5 **- The pin used to establish the direction the stepper motor will rotate**

enable_pin: !ar7 **- The pin used to signal a stepper motor to be on or off**

step_distance: 0.0125 **- The distance travelled during each step made by the stepper motor**

endstop_pin: tmc2130_stepper_x:virtual_endstop **-This defines the pin that will detect that an endstop has been triggered. In our case we are using sensorless homing and thus the definition of the pin name is different. Use earlier examples if using a physical endstop switch**

position_endstop: -10 **- The distance/position the endstop resides at. It must be a value between position min and max**

position_min: -20 **- The distance/position of the min/end of the printer movement range. The motors will stop if this position is reached** 

position_max: 200 **- The distance/position of the max/end of the printer movement range.**

homing_speed: 30 **- The speed at which the motor will move during the homing sequence**

homing_retract_dist: 0 **- The distance the motor will restract once homing has been completed. Due to issues with klipper/sensorless 
homing this variable is usually set to 0**

**NOTE: See the following to define a stepper motor associated with an additional MCU. Note the definition of the pins is different and matches the name of our Y axis MCU - yboard**

---

[stepper_y]

step_pin: yboard:ar6

dir_pin: !yboard:ar5

enable_pin: !yboard:ar7

step_distance: 0.0125

endstop_pin: tmc2130_stepper_y:virtual_endstop

position_endstop: -40

position_min: -50

position_max: 200

homing_speed: 10

homing_retract_dist: 0

---

* **Step 5:** **This section defines the stepper motor driver, the pins used to communicate with it, and specific values that are relative to you stepper motor. The below is the definition of our TMC2130 controlling our Y axis stepper motor.**

[tmc2130 stepper_y]


cs_pin: yboard:ar10 **- The chip select pin used by the arduino. This is used to conduct SPI communication. Please note that the arduino may or may not have specific pins used for SPI. Documentation varies for the arduino in this regard**

#spi_software_mosi_pin: yboard:ar11 **- The Master Out Slave In (mosi) pin**

#spi_software_miso_pin: yboard:ar12 **- The Master In Slave Out (miso) pin**

#spi_software_sclk_pin: yboard:ar13 **- The Serial Clock pin, used for timing the MCU, Stepper motor driver and stepper motor**

**NOTE: The SPI variable settings above are used only in Software SPI communication and is not needed**

interpolate: False **- This settings relates to the internal stepping of the motor. Default is true**

run_current: 0.8 **- The current in AMPS, the TMC2130 will supply the stepper motor when conducting movement**

hold_current: 0.75 **- The current in AMPS, used when the stepper motor has been enabled and is at stand still**

sense_resistor: 0.110 **- The resistance value of the internal sense resitor located on the TMC2130**

diag1_pin: ^!yboard:ar2 **- This is the pin used to signal a stall. It is used during sensorless homing. Please see the TMC2130 Data sheet for a description of where this is located. An additional connection must be established here e.g A soldered wire.The definition of the virtial endstop in the above section, enables this parameter/variable to be used**

microsteps: 16 **- The sub steps taken. This breaks one full step into smaller steps for more accurate control**

stealthchop_threshold: 200 **- Stealthchop is a Trinamic proprietary setting. It helps to silence the stepper motor during use**

driver_IHOLDDELAY: 8 **- This specifies the time delay before free wheeling of the motor is allowed**

driver_TPOWERDOWN: 0 **- This specifies the time taken before the motor will be powered down after use**

driver_TOFF: 4 **- This specifies the time the TMC2130 will wait before switching off the signal to the motor**

driver_HEND: 7 **- Hysteresis end value**

driver_HSTRT: 0 **-Hysteresis start value**

driver_TBL: 1 **- This setting incorporates blank time**

driver_PWM_AUTOSCALE: True **- Enables auto scaling of the current based on velocity of the motor**

driver_PWM_FREQ: 1 **- This defines the PWM Frequency - TMC2130 Data sheet**

driver_PWM_GRAD: 4 **- This defines the PWM Gradient for velocity based scaling - See TMC2130 data sheet**

driver_PWM_AMPL: 128 **- This the amplitude/offset for entering stealthchop - See TMC2130 data sheet** 

driver_SGT: 40 **- This value is crucial when configuring Sensorless homing. It specifies the wait time/resistance encountered by the stepper motor before signalling a stall has been detected. Stallguard is a Trinamic proprietary setting**

---

* **Step 6: Define you Printer Kinematic Profile**

A printer kinematic profile must be provided. Different profiles require different pin/mcu labelling and definition of different parameters. Our project uses the cartesian profile which we will change to corexy in future. See the [Klipper Git](https://github.com/KevinOConnor/klipper/tree/master/config) which defines the many kinematic profiles and gives examples of the required parameters.

***Kinematic profile example***

[printer] #- Defines the printer configuration section 
kinematics: cartesian #- Sets kinematic profile 
max_velocity: 10 #- Sets max velocity 
max_accel: 30 #- Sets max acceleration.
square_corner_velocity: 5.0
   The maximum velocity (in mm/s) that the toolhead may travel a 90
   degree corner at. A non-zero value can reduce changes in extruder
   flow rates by enabling instantaneous velocity changes of the
   toolhead during cornering. This value configures the internal
   centripetal velocity cornering algorithm; corners with angles
   larger than 90 degrees will have a higher cornering velocity while
   corners with angles less than 90 degrees will have a lower
   cornering velocity. If this is set to zero then the toolhead will
   decelerate to zero at each corner. The default is 5mm/s.


*Sections to come*
# extruder
# heaters
# fans 
# unused sections? e.g// Homing Overide





