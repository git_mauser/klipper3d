# Installing and configuring Klipper3d, Flashing ATmega328P on Raspberry Pi 4 with Octoprint OS

### Objective: To successfully use Klipper3d in conjunction with Octoprint

#### **Distributions:**

##### **Raspberry Pi:**
* Model: Raspberry Pi 4
* Distributor ID: Raspbian

#### **Octoprint:**
* Version: 1.4.0

#### **Arduino:**
* Model: Arduino Nano
* Version: ATmega328P
* CPU: 16 mHz
* Connection: Serial USB cable
* Amount: 3

#### **Guide:**

* **Step 1:** Install Octoprint OS (See guide) - [Link](https://gitlab.com/git_mauser/raspi_install/-/blob/master/How%20to%20install%20Octoprint%20on%20Raspberry%20Pi%204.md)

* **Step 2:** Download Klipper `git clone https://github.com/KevinOConnor/klipper`

* **Step 3:** Run the Octoprint install script `./klipper/scripts/install-octopi.sh`

* **Step 4:** Change directory into klipper `cd ~/klipper/`

* **Step 5:** Stop the Klipper Service `sudo service klipper stop`

* **Step 6:** Run the command to configure elf.hex file for flashing `make menuconfig`

> This is an important step but for now we will use the basic setup:
> * DO NOT USE ADVANCED SETTINGS  
> * Under micro-controller select `Atmega AVR`
> * Under processor model select `atmega328p`
> * Press `Q` to exit and `Y` to save the configuration

* **Step 7:** Upon completing Step 5 execute the `make` command to create the necessary files

* **Step 8:** Connect your ATmega328P using a serial USB cable and ascertain which port it is connected on - 
    `ls -la /dev.serial/by-id/*`

* **Step 9:** Once Step 7 is complete you can now flash the ATmega328P with - 
    `avrdude -p ATmega328P -c arduino -b 57600 -P /dev/tty<your_port_number_here> -U out/klipper.elf.hex`


* **Step 10:** Ensure the on-screen process completes successfully with no errors. If errors occur their may be an issue with the ATmega328P
* **Step 11:** Repeat Step 9 on the remaining ATmega328P units ensuring the USB serial port is correct.

* **Step 12:** Restart the Klipper service `sudo service klipper start`

## The proccess of installing Klipper and flashing you ATmega328P set should now be complete. See the Printer.cfg guide for the next step in setting up Klipper for use with Octoprint - [Link](Remember to insert weblink here)


